# Binar challege chapter 6
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

1. Buatlah monolith dashboard sederhana dengan menggunakan view engine.
2. Super User Authentication login dashboard menggunakan data statis.
3. Membuat minimal 3 tabel contohnya:
  ● Tabel user_game
  ● Tabel user_game_biodata
  ● Tabel user_game_history
4. Data pada monolith dashboard sudah terkoneksi dengan database SQL dan memiliki metode sebagai berikut:
  ● Create data user game
  ● Read data user game
  ● Update data user game
  ● Delete data user game
5. Merancang dan mengimplementasikan skema database yang mengaitkan ketiga tabel tersebut dengan user_id sebagai Parent Key(PK).
6. Membuat RESTFUL API satu endpoint dengan methods CRUD (create, read, delete, update).
7. Database attribute ditentukan sendiri oleh siswa.

