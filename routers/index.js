const express = require('express')
const router = express.Router();
const Controller = require('../controllers')
const fs = require('fs')

let isLogin = false
let playerEmail ='';

router.get('/', Controller.home);

router.get('/trial', Controller.trial);

router.get('/login', Controller.login);
router.post('/login', Controller.postLogin)

router.post('/api/v1/login', Controller.actionLogin)

router.get('/dashboard', Controller.dashboard);
router.get('/dashboard/register', Controller.register);
router.get('/dashboard/users', Controller.userList);
router.get('/dashboard/user/:id/edit', Controller.userEdit);
router.delete('/dashboard/user/:id/delete', Controller.userDelete);


module.exports = router;